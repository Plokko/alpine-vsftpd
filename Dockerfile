###########################################################
##			Alpine vsFTP server
###########################################################
FROM alpine:3.7

ENV FTP_USER=ftp \
    FTP_PASS=ftp-password \
    FTP_UID=1000 \
    FTP_GID=1000 \
    FTP_HOME=/home/ftp \
    PASV_ADDRESS=vsftp.address \
    PASV_MIN=21100 \
    PASV_MAX=21110


## Upgrade & install
RUN apk upgrade --update --no-cache && apk add --no-cache vsftpd

#  Configure vsftpd

RUN echo "local_enable=YES" >> /etc/vsftpd/vsftpd.conf \
  && echo "chroot_local_user=YES" >> /etc/vsftpd/vsftpd.conf \
  && echo "allow_writeable_chroot=YES" >> /etc/vsftpd/vsftpd.conf \
  && echo "background=NO" >> /etc/vsftpd/vsftpd.conf \
  && echo "ftpd_banner=Welcome to FTP Server" >> /etc/vsftpd/vsftpd.conf \
  && echo "dirmessage_enable=YES" >> /etc/vsftpd/vsftpd.conf \
  && echo "max_clients=10" >> /etc/vsftpd/vsftpd.conf \
  && echo "max_per_ip=5" >> /etc/vsftpd/vsftpd.conf \
  && echo "write_enable=YES" >> /etc/vsftpd/vsftpd.conf \
  && echo "local_umask=022" >> /etc/vsftpd/vsftpd.conf \
  && echo "passwd_chroot_enable=yes" >> /etc/vsftpd/vsftpd.conf \
  && echo "pasv_enable=Yes" >> /etc/vsftpd/vsftpd.conf \
  && echo "listen_ipv6=NO" >> /etc/vsftpd/vsftpd.conf \
  && echo "seccomp_sandbox=NO" >> /etc/vsftpd/vsftpd.conf \
  && sed -i "s/anonymous_enable=YES/anonymous_enable=NO/" /etc/vsftpd/vsftpd.conf



COPY ./entrypoint.sh /entrypoint.sh
RUN chmod +x /entrypoint.sh

###################
## Remove apk cache
RUN rm -R /var/cache/apk/*


EXPOSE 21 21100-21110

# Run vsftp entrypoint
CMD /entrypoint.sh

